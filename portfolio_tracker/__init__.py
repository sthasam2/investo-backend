from ..share_calculator import Share_Calculator
from ..options import SHARE_TYPES, SHARE_VALUE, TRANSACTION_TYPE

calc = Share_Calculator


class Transaction:
    transaction_detail = dict(
        company=None,
        date=None,
        action=None,
        quantity=None,
        buy_price=None,
        sell_price=None,
        share_type=None,
        charges=None,
        total_price=None,
        unit_charged_price=None,
    )

    def _check_buy_or_sell(action: str):

        return

    def _check_keys(provided: dict):
        req = ["company", "date", "action", "share_type", "quantity"]

        if provided.action == TRANSACTION_TYPE["BUY"]:
            req.append("buy_price")
        elif provided.action == TRANSACTION_TYPE["SELL"]:
            req.append("sell_price")

        prov = [keys for keys in provided]
        if req.sort() != prov.sort():
            return False
        else:
            return True

    def _check_values(provided: dict):

        return True

    def _check_transaction_details(self, transaction_details):
        try:
            if type(transaction_details) is not dict:
                raise TypeError

            if not self.check_keys():
                raise KeyError

            if not self.check_values():
                raise ValueError

            return True

        except TypeError:
            print(f"The entered argument must be a dictionary")
            return False

        except KeyError:
            print(f"Entered dictionary must contain")
            print(f'"company", "date", "price", "share_price", "action"')
            return False

    def __init__(self, transaction_details: dict = None) -> dict:
        if self._check_transaction_details(transaction_details):
            for key, value in transaction_details.items():
                self.transaction_detail[key] = value
        else:
            print("Error initializing")

    def _get_charge(self, transaction_details: dict) -> float:
        if transaction_details["action"] == TRANSACTION_TYPE["BUY"]:
            final_price = calc.buy_amount(
                transaction_details["number"], transaction_details["price"]
            )
            self.charged_price = final_price["cost_per_share"]
            self.total_price = final_price["total_price"]

    def add_buy(self, **kwargs):
        return True

    def add_sell(self, transaction_details: dict):
        return
