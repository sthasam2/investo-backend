INSERT INTO
    { self.news_tablename } (
        source,
        headline,
        subtitle,
        date,
        nepdate,
        scraped_datetime,
        image_url,
        url,
        scrip
    )
VALUES
    (
        item ["source"] [0],
        item ["image_url"] [0],
        item ["headline"] [0],
        item ["subtitle"] [0],
        item ["date"] [0],
        item ["nepdate"] [0],
        item ["scraped_datetime"] [0],
        item ["url"] [0],
    )