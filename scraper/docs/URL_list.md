# newweb url list

## market summary

-   https://newweb.nepalstock.com/api/nots/market-summary/

## turnover

-   https://newweb.nepalstock.com/api/nots/top-ten/turnover

## volume

-   https://newweb.nepalstock.com/api/nots/top-ten/trade

## transaction

-   https://newweb.nepalstock.com/api/nots/top-ten/transaction

## nepse-data

-   https://newweb.nepalstock.com/api/nots/nepse-data/market-open

# floorsheet

-   https://newweb.nepalstock.com/api/nots/nepse-data/floorsheet?&sort=contractId,desc

## company list

-   https://newweb.nepalstock.com/api/nots/company/list

## top gainers

-   https://newweb.nepalstock.com/api/nots/top-ten/top-gainer

## top loser

-   https://newweb.nepalstock.com/api/nots/top-ten/top-loser

## supply demand

-   https://newweb.nepalstock.com/api/nots/nepse-data/supplydemand

## nepse index

### nepse day index 58

### nepse sensitive 57

### nepse float 62

### sensitive float 63

-   https://newweb.nepalstock.com/api/nots/graph/index/58
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=58&startDate=2007-09-14&endDate=2020-12-19

### nepse life insurance index 65

-   https://newweb.nepalstock.com/api/nots/graph/index/65
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=65&startDate=2007-09-14&endDate=2020-12-19

### nepse nonlife 59

-   https://newweb.nepalstock.com/api/nots/graph/index/59
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=59&startDate=2007-09-14&endDate=2020-12-19

### manufacturing and processing 56

-   https://newweb.nepalstock.com/api/nots/graph/index/56
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=56&startDate=2007-09-14&endDate=2020-12-19

### microfinance 64

-   https://newweb.nepalstock.com/api/nots/graph/index/64
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=64&startDate=2007-09-14&endDate=2020-12-19

### development bank 55

-   https://newweb.nepalstock.com/api/nots/graph/index/55
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=55&startDate=2007-09-14&endDate=2020-12-19

### trading 61

-   https://newweb.nepalstock.com/api/nots/graph/index/61
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=61&startDate=2007-09-14&endDate=2020-12-19

### hotels 52

-   https://newweb.nepalstock.com/api/nots/graph/index/52
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=52&startDate=2007-09-14&endDate=2020-12-19

### others 53

-   https://newweb.nepalstock.com/api/nots/graph/index/53
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=53&startDate=2007-09-14&endDate=2020-12-19

### hydro 54

-   https://newweb.nepalstock.com/api/nots/graph/index/54
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=54&startDate=2007-09-14&endDate=2020-12-19

### banking 51

-   https://newweb.nepalstock.com/api/nots/graph/index/51
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=51&startDate=2007-09-14&endDate=2020-12-19

### finance 60

-   https://newweb.nepalstock.com/api/nots/graph/index/60
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=60&startDate=2007-09-14&endDate=2020-12-19

### mutual fund 66

-   https://newweb.nepalstock.com/api/nots/graph/index/66
-   https://newweb.nepalstock.com/api/nots/graph/index?indexCode=66&startDate=2007-09-14&endDate=2020-12-19

## brokers

-   https://newweb.nepalstock.com/api/nots/member?&size=50

Note, check members to see if changes

## securities

-   https://newweb.nepalstock.com/api/nots/security?nonDelisted=true

### particular scrip

-   https://newweb.nepalstock.com/api/nots/security/131
-   floorsheet: https://newweb.nepalstock.com/api/nots/security/floorsheet/131?&businessDate=2020-12-17&sort=contractId,asc
-   pricehistory: https://newweb.nepalstock.com/api/nots/security/floorsheet/131?&businessDate=2020-12-17&sort=contractId,asc

## todays price

-   https://newweb.nepalstock.com/api/nots/nepse-data/today-price

## market depth

-   https://newweb.nepalstock.com/api/nots/nepse-data/marketdepth/255
