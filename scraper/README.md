# Investoscraper

Scrapy scrapers for different data sources.

## Content

1. ### **definitions**

   Consists wiki of constant data types and items

2. ### **nepsescraper**

   Scraper utility for scraping [Newweb Nepalstock](https://newweb.nepalstock.com/) source. It scrapes Market Data

3. ### **newsscraper**

   Scraper utility for scraping news portals. It scrapes recent News.

4. ### **oldnepsescraper**

   Scraper utility for scraping [Nepalstock](https://nepalstock.com/)  source. It scrapes Market Data

5. ### **sebonscraper**

   Scraper utility for scraping [SEBON](http://sebon.gov.np/) source. It different issues of sebon.
