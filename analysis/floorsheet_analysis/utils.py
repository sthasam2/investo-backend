import os
import psycopg2


def get_environ_variable(var_name: str) -> str:
    """
    Get the environment variable from your os or return an exception
    """
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = f"{var_name} not found!\nSet the '{var_name}' environment variable"
        raise KeyError(error_msg)


class DBPostgresHelper:
    """"""

    def __init__(self):
        """
        initialization methood
        """
        # Database Postgres
        self.DB_NAME = get_environ_variable("PSQL_NAME")
        self.DB_USER = get_environ_variable("PSQL_USER")
        self.DB_PW = get_environ_variable("PSQL_PASSWORD")
        self.DB_HOST = get_environ_variable("PSQL_HOST")
        self.DB_PORT = get_environ_variable("PSQL_PORT")

        self.connection = None
        self.cursor = None

    def _create_connection(self):
        """
        create database connection
        """
        try:
            self.connection = psycopg2.connect(
                user=self.DB_USER,
                password=self.DB_PW,
                host=self.DB_HOST,
                database=self.DB_NAME,
                port=self.DB_PORT,
            )

            self.cursor = self.connection.cursor()

            # print information
            print(
                "Postgres Server information:\n", self.connection.get_dsn_parameters()
            )

            self.cursor.execute("SELECT version();")

            print("\nYou are connected to", self.cursor.fetchone(), "\n")

        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL\n", error)

    def _disconnect(self):
        """
        disconnect from database
        """
        if self.connection:
            self.cursor.close()
            self.connection.close()
            print("PostgreSQL connection is closed")

    def _check_table_existence(self, tablename):
        """ """

        self.cursor.execute(
            f"""SELECT EXISTS(SELECT * FROM pg_tables WHERE tablename='{self.news_tablename}');"""
        )
        return self.cursor.fetchone()[0]

    def _create_table(self, tablename):
        """
        Create table
        """

        exists = self._check_table_existence(tablename)
        # if not exists:
        #     create_table_query = f"""CREATE TABLE {self.tablename}(
        #             source VARCHAR (50),
        #             headline VARCHAR (300),
        #             subtitle TEXT,
        #             date VARCHAR (100),
        #             nepdate VARCHAR (100),
        #             scraped_datetime VARCHAR (100),
        #             image_url VARCHAR (200),
        #             url VARCHAR (200),
        #             scrip VARCHAR []
        #     )"""
        #     self.cursor.execute(create_table_query)

    def _store_db(self, item):
        """
        Stores in database
        """
        sql = f"""INSERT INTO
            {self.tablename} (source,headline,subtitle,date,nepdate,scraped_datetime,image_url,url,scrip)
        VALUES
            (%s, %s, %s, %s, %s, %s, %s,%s,%s);
        """
        data = (
            item["source"],
            item["headline"],
            item["subtitle"],
            item["date"],
            item["nepdate"],
            item["scraped_datetime"],
            item["image_url"],
            item["url"],
            item["scrip"],
        )

        self.cursor.execute(sql, data)
        self.connection.commit()

    def process_item(self, item, spider):
        self._store_db(item)
        return item
