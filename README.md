# Investo

A tech focused on making your investing journey more refined and easy.

## Changelog

Check for changelogs at `changelogs.md` .

## Setup

### Dependedencies

#### **Main Dependencies**

*   **Python 3.8**
*   **pip** ( python default package manager)
*   **poetry** ( Package and virtualenv manager | `pip install poetry`)

To setup, after all the mentioned dependencies are installed, run command:

``` python
poetry install
```

This will install all the required dependencies for the project using the **poetry.lock** file which are needed for the projects.  
  
To check more dependencies enter command 

``` python
poetry show
```
