"""share_calculator.py"""

import math
from datetime import date

from humps import camelize, decamelize


class Share_Calculator:
    """
    Share calculator class. Consisits of all the definitions fror calcualtions regarding share.

    METHODS
    ---
    PORTFOLIO SHARE PRICE ADJUSTMENTS
    - _wacc_price(self, holding_shares: list) -> dict:
        Calculates the WACC price from a given list containing [{"type":,"number":,"buy_price":}]

    FEES/ COMMISSIONS
    - _broker_commission(self, chargefree_total_amount: float, tran_date: str = None) -> dict:
        Gives broker commission rates and total commission amount
    - _sebon_fees(self, chargefree_total_amount: float) -> dict:
        Gives sebon_fees and other charges

    TOTAL PRICES
    - _chargefree_total_amount(self, share_quantity: int, buy_sell_price: float) -> float:
    - _charged_total_amount(self, chargefree_total_amount: float) -> float:

    CAPITAL GAIN/LOSS
    - _capital_gain_tax(self, sell_share_quantity: int, sell_price: float, wacc_buying_price: float) -> dict:
        Calculates the total tax amount for profit
    - _check_profit_loss(self, sell_details: dict) -> dict:
        Calculates the profit amount if profit and returns status and profit

    SECONDARY BUY/SELL
    - buy_amount(self, share_quantity: int, unit_buy_price: float) -> dict:
        Gives the buy transaction details for given sell of shares
    - sell_amount(self, share_quantity: int, unit_sell_price: float, unit_buy_price: float) -> dict:
        Gives the sell transaction details for given sell of shares

    ADJUSTMENTS
    - bonus_adjustment(self, bonus_percent: float, closing_price: float) -> dict:
        calculate price after bonus adjustment
    - right_adjustment(self, right_percent: float, closing_price: float, base_price: float) -> dict:
        calculate price after right adjustment

    BONUS
    - min_buy_for_given_bonus(self, req_bonus_share: int, bonus_percent: float) -> dict:
        Calculates the Minimum number of shares needed to buy for given number of bonus shares from bonus percentage
    - bonus_for_given_share(self,holding_shares: int, bonus_percent: float = 0, divident_percent: float = 0, base_price: float = 0, ) -> dict:
        Calculates the bonus and divident obtained for given rates and holding shares
    """

    ####################################################
    # PORTFOLIO SHARE PRICE ADJUSTMENTS
    ####################################################

    def _wacc_price(self, holding_shares: list) -> dict:
        """
        Calculates the WACC price from a given list containing [{"type":,"number":,"buy_price":}]

        PARAMETERS
        ---
        - holding_shares : list a list containing dictionary items corresponding to {"type":,"number":,"buy_price":}

        RETURNS
        ---
        dict :
        """
        num_pro, div_sum = 0, 0

        for share in holding_shares:
            num_pro += share["number"] * share["buy_price"]
            div_sum += share["number"]

        wacc = {"wacc_adjusted_price": round((num_pro / div_sum), 2)}

        return wacc

    ###################################################
    # FEES/ COMMISSIONS
    ###################################################

    def _broker_commission(
        self, chargefree_total_amount: float, tran_date: str = None
    ) -> dict:
        """
        Gives broker commission rates and total commission amount

        PARAMETERS
        ---
        - chargefree_total_amount (float) : total value obtained by multiplying share_quantity and unit buy/sell price
        - tran_date (str): OPTIONAL must be in format YYYY-MM-DD
        RETURNS
        ---
        dict: a dictionary containing { "rate": ... , "commission_amount": ... }
        """
        # if tran_date is not None & (
        #     date.fromisoformat(tran_date) < date.fromisoformat("2016-07-22")
        # ):
        #     commission_rate = {
        #         "50k": 0.01,
        #         "50k_5l": 0.009,
        #         "5l_10l": 0.008,
        #         "10l": 0.007,
        #     }
        if tran_date is not None & (
            date.fromisoformat(tran_date) < date.fromisoformat("2020-12-24")
        ):
            commission_rate = {
                "50k": 0.006,
                "50k_5l": 0.0055,
                "5l_20l": 0.005,
                "20l_1c": 0.0045,
                "1c": 0.004,
            }
        else:
            commission_rate = {
                "50k": 0.004,
                "50k_5l": 0.0037,
                "5l_20l": 0.0034,
                "20l_1c": 0.0030,
                "1c": 0.0027,
            }

        rate = commission_rate["50k"]
        commission_amount = chargefree_total_amount * rate

        if commission_amount < 10:
            commission_amount = 10
        else:
            if chargefree_total_amount <= 50000:
                rate = commission_rate["50k"]
            elif 50000 < chargefree_total_amount <= 500000:
                rate = commission_rate["50k_5l"]
            elif 500000 < chargefree_total_amount <= 2000000:
                rate = commission_rate["5l_20l"]
            elif 2000000 < chargefree_total_amount <= 10000000:
                rate = commission_rate["20l_1c"]
            elif 10000000 < chargefree_total_amount:
                rate = commission_rate["1c"]

            commission_amount = round((rate * chargefree_total_amount), 2)

        commission = {
            "rate": rate * 100,
            "commission_amount": commission_amount,
        }
        return commission

    def _sebon_fees(self, chargefree_total_amount: float) -> dict:
        """
        Gives sebon_fees and other charges

        PARAMETERS
        ---
        - chargefree_total_amount (float) : total value obtained by multiplying share_quantity and unit buy/sell price

        RETURNS
        ---
        dict: a dictionary containing {
            "dp_fee": ...,
            "sebon_fee": ...,
            "total": ...)
        }
        """
        fees = {
            "dp_fee": 25,
            "sebon_fee": round((chargefree_total_amount * 0.0006), 2),
            "total": round((25 + (chargefree_total_amount * 0.0006)), 2),
        }
        return fees

    ####################################################
    # total_price
    ####################################################

    def _chargefree_total_amount(
        self, share_quantity: int, buy_sell_price: float
    ) -> float:
        """
        Calculates the chargeless price for face value of shares i.e. product of share and its marked price

        PARAMETERS
        ---
        - share_quantity (int) :number of shares
        - buy_sell_price (float) :price at which share is bought/sold

        RETURNS
        ---
        float: chargeless amount for sell/buy

        """
        return share_quantity * buy_sell_price

    def _charged_total_amount(self, chargefree_total_amount: float) -> float:
        """
        Calculates the charged price for face value of shares
        i.e. product of share and its marked price plus all the fees, commissions except capital gain taxes

        PARAMETERS
        ---
        - share_quantity (int) :number of shares
        - buy_sell_price (float) :price at which share is bought/sold

        RETURNS
        ---
        float: chargeless amount for sell/buy

        """
        total = (
            chargefree_total_amount
            + self._broker_commission(chargefree_total_amount)["commission_amount"]
            + self._sebon_fees(chargefree_total_amount)["total"]
        )
        return total

    ####################################################
    # CAPITAL GAIN/LOSS
    ####################################################

    def _check_profit_loss(self, sell_details: dict) -> dict:
        """
        Calculates the profit amount if profit and returns status and profit

        PARAMETERS
        ---
        - sell_details (dict) : a dictionary containing sell details
        {"sell_share_quantity": ..., "sell_price": ..., "wacc_buying_price": ... }

        RETURNS
        ---
        dict : dictionary with content {"status": ..., "amount": ...}
        """
        chargefree_total_amount = self.chargefree_total_amount(
            sell_details["sell_share_quantity"], sell_details["sell_price"]
        )
        buy_amount = (
            sell_details["wacc_buying_price"] * sell_details["sell_share_quantity"]
        )
        sell_amount = self._charged_total_amount(chargefree_total_amount)

        if buy_amount < sell_amount:
            return {"status": "Profit", "amount": (sell_amount - buy_amount)}
        elif sell_amount < buy_amount:
            return {"status": "Loss", "amount": (sell_amount - buy_amount)}
        else:
            return {"status": "Unchanged", "amount": 0}

    def _capital_gain_tax(
        self, sell_share_quantity: int, sell_price: float, wacc_buying_price: float
    ) -> dict:
        """
        Calculates the total tax amount for profit

        PARAMETERS
        ---
        - sell_share_quantity (int) : number of shares sold
        - sell_price (float) : market price at which share sold
        - wacc_buying_price (float) : unit price at which given share was bought incluing fees and commissions

        RETURNS
        ---
        dist: dictionary containing {
            "sell_share_quantity": ...,
            "sell_price": ...,
            "wacc_buying_price": ...
        }
        """
        sell_details = {
            "sell_share_quantity": sell_share_quantity,
            "sell_price": sell_price,
            "wacc_buying_price": wacc_buying_price,
        }
        profit_loss = self._check_profit_loss(sell_details)
        capital_tax = 0

        if profit_loss["status"] == "Profit":
            capital_tax = round((profit_loss["amount"] * 0.05), 2)

        return {
            "status": profit_loss["status"],
            "amount": profit_loss["amount"],
            "capital_tax": capital_tax,
        }

    ####################################################
    # SECONDARY BUY/SELL
    ####################################################

    def buy_amount(self, share_quantity: int, unit_buy_price: float) -> dict:
        """
        Gives the buy transaction details for given sell of shares

        PARAMETERS
        ---
        - share_quantity (int): number of shares
        - unit_buy_price (float): bought price for one share

        RETURNS
        ---
        dict: Dictionary containing
        {
            "quantity": ...,
            "buyPrice": ...,
            "brokerFee": ...,
            "sebonFee": ...,
            "dpFee": ...,
            "costPerShare": ...,
            "totalPrice":...
        }
        """
        chargefree_total_amount = self.chargefree_total_amount(
            share_quantity, unit_buy_price
        )

        broker_fee = self._broker_commission(chargefree_total_amount)
        sebon_fee = self._sebon_fees(chargefree_total_amount)

        charged_total_amount = (
            chargefree_total_amount
            + broker_fee["commission_amount"]
            + sebon_fee["total"]
        )

        buy_details = {
            "quantity": share_quantity,
            "buy_price": unit_buy_price,
            "broker_fee": broker_fee["commission_amount"],
            "sebon_fee": sebon_fee["sebon_fee"],
            "dp_fee": sebon_fee["dp_fee"],
            "cost_per_share": round((charged_total_amount / share_quantity), 2),
            "total_price": charged_total_amount,
        }
        return camelize(buy_details)

    def sell_amount(
        self, share_quantity: int, unit_sell_price: float, unit_buy_price: float
    ) -> dict:
        """
        Gives the sell transaction details for given sell of shares

        PARAMETERS
        ---
        - share_quantity (int): number of shares
        - unit_sell_price (float): selling price for one share
        - unit_buy_price (float): bought price for one share

        RETURNS
        ---
        dict: Dictionary containing
        {
            "quantity": ...,
            "sellPrice": ...,
            "brokerFee": ...,
            "sebonFee": ...,
            "dpFee": ...,
            "capitalGain": ...,
            "costPerShare": ...,
            "totalPrice": ...,
        }
        """
        # sell
        chargefree_sell = self.chargefree_total_amount(
            share_quantity, unit_sell_price
        )  # chargeless amount
        # buy
        unit_charged_buy_price = (
            self.buy_amount(share_quantity, unit_buy_price)["cost_per_share"]
            / share_quantity
        )  # unit price of charged amount
        # fees
        broker_fee = self._broker_commission(chargefree_sell)
        sebon_fee = self._sebon_fees(chargefree_sell)
        # tax
        capital_tax = self._capital_gain_tax(
            share_quantity, unit_sell_price, unit_charged_buy_price
        )

        total_amount = (
            chargefree_sell
            - broker_fee["commission_amount"]
            - sebon_fee["total"]
            - capital_tax["capital_tax"]
        )

        sell_details = {
            "quantity": share_quantity,
            "sell_price": unit_sell_price,
            "broker_fee": broker_fee["commission_amount"],
            "sebon_fee": sebon_fee["sebon_fee"],
            "dp_fee": sebon_fee["dp_fee"],
            "capital_gain": capital_tax["capital_tax"],
            "cost_per_share": round((total_amount / share_quantity), 2),
            "total_price": round(total_amount, 2),
        }

        return camelize(sell_details)

    ####################################################
    # ADJUSTMENTS
    ####################################################

    def bonus_adjustment(self, bonus_percent: float, closing_price: float) -> dict:
        """
        calculate price after bonus adjustment

        PARAMETERS
        ---
        - bonus_percent (float) :percentage of bonus given
        - closing_price (float) :previous closing price of the share

        RETURNS
        ---
        dict: dictionary { "bonusPercentage": ..., "adjustedPrice": ...}
        """
        adjustment = {
            "bonus_percentage": bonus_percent,
            "adjusted_price": round(((closing_price * 100) / (100 + bonus_percent)), 2),
        }
        return camelize(adjustment)

    def right_adjustment(
        self, right_percent: float, closing_price: float, base_price: float
    ) -> dict:
        """
        calculate price after right adjustment

        PARAMETERS
        ---
        - right_percent (float) :percentage of right given
        - closing_price (float) :previous closing price of the share
        - base_price    (float) :base price of the share

        RETURNS
        ---
        dict: dictionary item containing "right percentage" and "adjusted price"
        """
        adjustment = {
            "right_percentage": right_percent,
            "adjusted_price": round(
                (
                    (closing_price + (base_price * (right_percent / 100)))
                    * 100
                    / (100 + right_percent)
                ),
                2,
            ),
        }
        return camelize(adjustment)

    ####################################################
    # BONUS
    ####################################################

    def min_buy_for_given_bonus(
        self, req_bonus_share: int, bonus_percent: float
    ) -> dict:
        """
        Calculates the Minimum number of shares needed to buy for
        given number of bonus shares from bonus percentage

        PARAMETERS
        ---
        - req_bonus_share   (int) :required number of shares
        - bonus_percent     (float) :percentage of bonus offered

        RETURNS
        ---
        dict : A dictionary containing
        {"bonusPercent": ..., "reqShares": ..., "minShareToBuy":...}
        """
        min_shares = {
            "bonus_percent": bonus_percent,
            "req_shares": req_bonus_share,
            "min_share_to_buy": math.ceil((100 * req_bonus_share) / bonus_percent),
        }
        return camelize(min_shares)

    def bonus_for_given_share(
        self,
        holding_shares: int,
        bonus_percent: float = 0,
        divident_percent: float = 0,
        base_price: float = 0,
    ) -> dict:
        """
        Calculates the bonus and divident obtained for given rates and holding shares

        PARAMETERS
        ---
        - req_bonus_share   (int) :required number of shares
        - bonus_percent     (float) :percentage of bonus offered

        RETURNS
        ---
        dict : A dictionary containing {"bonusPercent": ..., "reqShares": ..., "minShareToBuy":...}
        """
        bonus = {
            "holding_shares": holding_shares,
            "bonus_percent": bonus_percent,
            "bonus_shares": math.floor(holding_shares * (bonus_percent / 100)),
            "divident_amount": round(
                ((holding_shares * (divident_percent / 100) * base_price)), 2
            ),
        }
        return camelize(bonus)
