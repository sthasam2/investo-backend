SHARE_TYPES = dict(
    AUC="Auction",
    IPO="Initial Public Offering (IPO)",
    FPO="Further Public Offering (FPO)",
    RIGHT="Right Share",
    BONUS="Bonus Share",
    ORDINARY="Secondary Share",
    DEB="Debentures",
    MUTF="Mutual Funds",
)

SHARE_VALUE = dict(
    IPO=100,
    FPO=100,
    RIGHT=100,
    BONUS=100,
    ORDINARY=None,
    AUC=None,
    DEB=1000,
    MUTF=10,
)

TRANSACTION_TYPE = dict(BUY="BUYING", SELL="SELLING")
