# Changelogs

## v.0.1.4.2: Add scraping codes, Refactored code, Utility functions

1. Added insurance khabar scraper
2. Refactored code to make same Newsscraper Item and addition of date fields
3. Addition of datetime utility functions
4. Added pipelines for inserting into PostgresdSQL database
5. Added documentations

## v.0.1.4.1: Refactored code, Code additions

1. Date utility and cleaner functions added
2. Completed new source--insurancekhabar

## v.0.1.4: Refactored code, Code additions

1. Refactored all newsscraper spiders
2. Added Date utility functions including nepali datetime
3. Unified Newsscraper item to one item
4. Added new source--insurancekhabar (not completed)

## v.0.1.3: Restructured, additions

1. Added Newsscrapers

   a. Bizpati.com

2. Modified Sebonscraper
3. Added Nepsescraper
4. Restructured directory structure.

   a. share_calculator is not in utils
   b. all scrapers moved to 'scrapers'

## v.0.1.3.1: Added additional sebonscrapers

1. Added debentures, MFS, public-issues-data

## v.0.1.3: Added newsscrapers

1. Added newsscrapers:

   a. Merolagani
   b. Sharesansar

## v.0.1.2.1: Chaged newsscraper directory structure

## v.0.1.2: Added Scraping modules

1. Added scrapy scraping modules for news of:

   a. Nepalipaisa
   b. Bizmandu
   c. Investopaper

## v.0.1.1: Added Sebon Scraper

1. Added sebon issue scrapers for ipo, fpo, right, bonus

## v.0.1.1: Additions, modifications, documentation, bugfixes

1. Adjusted sharecalculator, documentation
2. Started Portfolio tracker

## v.0.1.0: Added share calculator
